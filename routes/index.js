const express = require('express');
const router = express.Router()

module.exports = function() {
    
    router.get('/' , (req , resp) => {
        resp.send('Hola desde Router')
    });

    router.get('/nosotros' , (req , resp) => {
        resp.send('Hola desde nosotros')
    });


    return router;
}