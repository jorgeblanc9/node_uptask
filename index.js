// Dependecias
const express  = require('express');
const routers = require('./routes');

const app = express();


app.use('/', routers());

app.listen(3000 , () =>{
    console.log('Servidor Activo puerto: 3000');
});



